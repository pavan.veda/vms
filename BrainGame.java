package game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class BrainGame {

	public List<String> words;
	public List<String> gameList;
	public String turn;
	public int computerMoves;
	public Scanner scanner;

	public BrainGame() {
		words = Arrays.asList(new String[] { "a", "b", "c" });
		gameList = new ArrayList<>();
		turn = "Comp";
		computerMoves = 0;
		scanner = new Scanner(System.in);
	}

	public boolean computerTurn() {
		System.out.println("Computers Move");
		while (computerMoves < words.size()) {
			if (!gameList.contains(words.get(computerMoves))) {
				gameList.add(words.get(computerMoves));
				System.out.println("Computers new word '" + words.get(computerMoves) + "'");
				computerMoves += 1;
				return true;
			} else {
				computerMoves += 1;
			}
		}
		return false;
	}

	public boolean playerTurn() {
		System.out.println("Player Turn");
		for (String word : gameList) {
			System.out.println("Enter your word");
			String playerWord = scanner.next();
			if (!word.equals(playerWord)) {
				return false;
			}
		}
		System.out.println();
		return true;
	}

	public boolean start() {

		while (true) {

			if (!computerTurn()) {
				System.out.println("Player Won the game");
				return false;
			}
			if (!playerTurn()) {
				System.out.println("Computer won the game");
				return false;
			}

		}
	}

	public static void main(String[] args) {

		BrainGame brainGame = new BrainGame();
		System.out.println("Lets start brainGame");
		brainGame.start();
		System.out.println("Thanks for playing");
	}
}
